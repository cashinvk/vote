﻿const http = require('http');
 
var url=require('url');
var querystring = require('querystring');
var static = require('node-static');
var file = new static.Server('.');

function accept (req,res){
	if(req.url == '/kompot'){				
		incrementVote('kompot.txt',res);	
	}else	
	if(req.url == '/sok'){	
		incrementVote('sok.txt',res);			
	}	
	else{
		file.serve(req,res);
}}		
	http.createServer(accept).listen(8124);
	console.log('Server running at http://127.0.0.1:8124/');
		
function incrementVote(nameFile,res){
	var fs = require('fs');
		fs.readFile(''+nameFile, 'utf-8', function (error, data) {
			data = parseInt(data) + 1;
			fs.writeFile(nameFile, data);
			res.end('' + data);
		});	
}